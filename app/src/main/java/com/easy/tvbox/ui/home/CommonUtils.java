package com.easy.tvbox.ui.home;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CommonUtils {

    public static long date2TimeStamp(String beginDate) {
        SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());// "2019-06-13 00:00:00"
        Date d;
        try {
            d = simpleDateFormat.parse(beginDate);
            return d.getTime() + TimeZone.getDefault().getRawOffset();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static String timeStamp2Date(long startTime, int parameters) {
        SimpleDateFormat formatter = getSimpleDate(parameters);
        if (formatter != null) {
            Date date = new Date(startTime);
            return formatter.format(date);
        }
        return "";
    }

    public static SimpleDateFormat getSimpleDate(int parameters) {
        SimpleDateFormat formatter = null;
        switch (parameters) {
            case 0:
                formatter = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault());
                break;
            case 1:
                formatter = new SimpleDateFormat("yyyy.MM.dd;HH:mm", Locale.getDefault());
                break;
            case 2:
                formatter = new SimpleDateFormat("MM.dd;HH:mm", Locale.getDefault());
                break;
            case 3:
                formatter = new SimpleDateFormat("yyyy.MM.dd;HH:mm:ss", Locale.getDefault());
                break;
            case 4:
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                break;
            case 5:
                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                break;
            case 6:
                formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                break;
            case 7:
                formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
                break;
            case 8:
                formatter = new SimpleDateFormat("yyyyMM", Locale.getDefault());
                break;
            case 9:
                formatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
                break;
            case 10:
                formatter = new SimpleDateFormat("dd", Locale.getDefault());
                break;
            case 11:
                formatter = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                break;
            case 12:
                formatter = new SimpleDateFormat("yyyy-MM", Locale.getDefault());
                break;
            case 13:
                formatter = new SimpleDateFormat("MM-dd HH:mm:ss", Locale.getDefault());
                break;
            case 14:
                formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
                break;
            case 15:
                formatter = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
                break;

            case 16:
                formatter = new SimpleDateFormat("MM.dd", Locale.getDefault());
                break;
        }
        return formatter;
    }
}
